# Regression in the Presence of
Confounding Variables  

This work is the Final Project for Linear Statistical Models (STAT 208) at UCSC, where I study how to perform regression with contaminated data  

To view this work as a recorded presentation, click here: https://drive.google.com/file/d/1_RPdyfvUtwqjrvVfNxMASqzMYWikEtPa/view?usp=sharing

## Abstract

In this paper I review and critique Statistical Inference for Linear Regression models with
Additive Distortion Measurement Errors, which discusses methods of regression in environments where both the response variable and covariates have additive distortion measurement errors. In these cases, we observe data $\{\boldsymbol{\tilde{X}_i}, \tilde{Y}_i, \boldsymbol{U_i}\}$, $i = 1,...,n$, where true covariates $\boldsymbol{X_i}$ and response $Y_i$ are confounded by independent variable $U_i$. We verify the derivation of an efficient estimator for $\boldsymbol{\beta}$ and intercept $\boldsymbol{a}$ through a residuals-based technique, Efficient in this context refers to the additive distortion effects disappearing in the asymptotical variance, which is a notable advantage over the OLS estimator. We also review test statistics resulting from our residuals-based estimator that can be used for common hypothesis testing in regression settings. Simulation studies from the paper are recreated to show the improvement of this estimator over the OLS estimation as sample size increases.


**Keywords**: Residual-based Estimators, Asymptotic Properties, Additive Distortion 
